extends Node3D
@onready var pop = $Poppy
@onready var water = $Water
@onready var player = $Player
@export var rising = false
@onready var win = $win


func rising_level():
	rising = true 
	win.enable()
	
func reset():
	water.reset_position()
	rising = false
	win.disable()
	
func player_die():
	reset()
	print("DYING")

func player_won():
	reset()
	print("BRAVO")
	
func _ready():
	pop.angry.connect(rising_level)
	player.died.connect(player_die)
	player.won.connect(player_won)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if rising == true:
		water.position.y += 0.02
	
