extends Area3D
signal entered

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var joueurs = []
	for index in get_overlapping_bodies():
		# We get one of the collisions with the player
		if index.is_in_group("joueur"):
			joueurs.append(index)
			
	if joueurs.size() > 0 :
		for i in joueurs:
			print(i, "win")
			i.win()
