extends Area3D

@onready var text = get_node("text_int")
@onready var _first_position = text.global_position
@onready var _my_pnj = get_parent()
@export var _my_players = []
@export var player_entering = false
@export var has_player = false

# Called when the node enters the scene tree for the first time.

func _ready():
	text.visible = false
func reset():
	text.scale.x=1
	text.scale.y=1
	text.mesh.material.albedo_color = Color("c6c6c6")

func test(state):
	#text.material_override.albedo_color = Color(0, 0, 1) 
	if state == "holding":
		text.mesh.material.albedo_color = Color("d0ce00")
		text.scale.x=1.1
		text.scale.y=1.1
		
	else:
		
		_my_pnj.if_interact()
		reset()
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	has_player = false
	var _my_new_players = []
	for index in get_overlapping_bodies():
		# We get one of the collisions with the player
		if index.is_in_group("joueur"):
			_my_new_players.append(index)
	
	if _my_new_players.size() > 0:
		has_player = true
		text.visible = true
			#text.transform.LookAt(text.position - _camera_controller.main.transform.position);
			
			#text.rotate_object_local(Vector3(0,1,0), 3.14)

		for player in _my_new_players:
			var dif = _first_position - player.global_position
			text.global_position= _first_position - 0.5*dif
			text.look_at(_my_pnj.global_position)
			text.rotation.x=0
			text.position.y +=1.5
			if player._is_interacting == false:
				player.set_interacting(true)
				print("connect")
				player.interacting.connect(test)
				player_entering = true
		
	else:
		text.visible = false
		player_entering = false
		reset()
	
	# disconnect
	for i in _my_players:
		if i not in _my_new_players:
			print("disconnect")
			i.interacting.disconnect(test)
			i.set_interacting(false)
		
		
	_my_players = _my_new_players
