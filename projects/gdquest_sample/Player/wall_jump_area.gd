extends Area3D

@onready var _is_touching = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var touch = false
	for index in get_overlapping_bodies():
		# We get one of the collisions with the player
		if index.get_parent().is_in_group("terrain"):
			touch = true
	_is_touching = touch
func is_touching():
	return _is_touching
