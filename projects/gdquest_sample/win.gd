extends MeshInstance3D

# Called when the node enters the scene tree for the first time.
func _ready():
	disable()


func disable():
	process_mode = 4
	visible = false
func enable():
	process_mode = 0
	visible = true
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
