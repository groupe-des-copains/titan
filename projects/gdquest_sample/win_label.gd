extends Label
@onready var win_time: Timer = $win_timer

const HIDDEN_Y_POS := -100
const DISPLAY_Y_POS := 20


func _on_timeout() -> void:
	var tween := create_tween()
	tween.tween_property(self, "position:y", HIDDEN_Y_POS, 0.5)
	
	
func win_msg() -> void:
	if win_time.is_stopped():
		var tween := create_tween()
		tween.tween_property(self, "position:y", DISPLAY_Y_POS, 0.5)
	win_time.start()


# Called when the node enters the scene tree for the first time.
func _ready():
	win_time.timeout.connect(_on_timeout)

# Called every frame. 'delta' is the elapsed time since the previous frame
func _process(delta):
	pass
