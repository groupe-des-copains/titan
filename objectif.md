# Projet avec Loux

- Lucratif
- Minimum de graphismes, dernier moment
- Plutôt dans le jeu vidéo
- Utiliser un moteur, pouvoir bosser rapidement

## En grandes lignes

- Gamplay fun méga multi (buzz)
- Jeu tranquille à la undertale ?
- Slabs en 3D ???
- Lignes de Dialog, perso qui parlent en Sims, voir doublage si deter
- Plusieurs modes de jeux à terme? (battleroyal, moba)
- Ressemble à ce qu'on veut faire: https://store.steampowered.com/app/2054520/Gawr_Gura_Quest_for_Bread/

## Gameplay

### Brainstorming

- RogueLike
- Axé déplacements stylé
- Dash, Planeur, Grapin, Tp, Construction
- On commence avec rien stylé juste on dans quand une dalle va tomber
- Labyrinthe, Asile 
- Combats ?
- Les 2 vues (prio third ?)
- Persos différents à la slabs ? à voir
- Team de persos ? Chaque monde découvert perso +1
- Swap Dynamique à la Genshin
- Coop ???? : Principe de collectibles à récolter 
- Mob stun puis tué, à attaquer à plusieurs en même temps
- Versus, avec traqueur ???
- Pas bcp de capacités actives 2/3 par perso, bcp de passifs (attaque, déplacement, ulti ?)
- Mob à tuer, farming
- RogueLike RPGesque permanant
- Dalles qui tombent aléatoires dans tous les cas

- PROCEDURAL ????

### Clavier en main 

- Déplacement combat: Smash: Esquive aérienne, attaques qui ricochent, shield
- Déplacement niveau: Mirror edge: Parcours, marcher sur les murs, saltos
- Déroulement du jeu: Hades: Rogue Like

### Persos

- Perso de base, stats de base++ / dash / jump
- Perso Tp (Minato)
- Perso dimmensionnel (Nether)
- Perso temps (slow / arret) (The World)
- Perso Grapin (Livai)
- Perso tank gros marteau
- Perso Gravité (Pain)
- Perso construction Glace (Shoto)
- Perso saut + planeur

- etc.. ?

## Graphismes

- 3d retro pixelisé

### Jeux de référence

- https://store.steampowered.com/app/356670/Spookys_Jump_Scare_Mansion/
- https://store.steampowered.com/app/1804470/Duel_Corp/
- https://store.steampowered.com/app/2280/DOOM_1993/
- https://store.steampowered.com/app/406150/Refunct/?l=french

## Brainstorming histoire/univers

- Mignon joyeux qui cache un truc dark (à la Céleste)
- Persos sympas back dark story
- Dialogues mignons mais on comprend (à la Undertale)
- à la recherche de soi même, de plus en plus, parcours semé d'embuche
- Analogie avec une maladie type schyzo, alzheimer, dépression
- Persos = facette de personnalité (vice versa)

## Moyens techniques

- Moteur de jeu: https://godotengine.org/
- Graphismes: Blender

## Idées ++

- Start dans un sanctuaire du héros (elden ring, breath of the wild)
- On sait pas que c'est un rogue like jusqu'à la première mort